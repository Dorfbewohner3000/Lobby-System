package de.Dorfbewohner3000.Main;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

public class cmd_warp implements CommandExecutor{
    File file = new File("plugins/ALSystem/", "warps.yml");
    FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if(sender instanceof Player){
            Player p = (Player) sender;
            if(args.length == 1){
                if(p.hasPermission("Lobby.Warp."+args[0])){
                    if(!file.exists()){
                        try {
                            file.createNewFile();
                        }catch(IOException e){
                            e.printStackTrace();
                        }
                    }
                    if(cfg.isSet("Warps."+args[0]+".X")){
                        double x = cfg.getDouble("Warps."+args[0]+".X");
                        double y = cfg.getDouble("Warps."+args[0]+".Y");
                        double z = cfg.getDouble("Warps."+args[0]+".Z");
                        double yaw = cfg.getDouble("Warps."+args[0]+".YAW");
                        double pitch = cfg.getDouble("Warps."+args[0]+".PITCH");
                        Location targetLoc = new Location(p.getWorld(), x,y,z);
                        targetLoc.setYaw((float)yaw);
                        targetLoc.setPitch((float)pitch);
                        p.teleport(targetLoc);
                    }
                }
            }
        }
        return true;
    }
}
