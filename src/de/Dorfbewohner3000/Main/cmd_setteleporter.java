package de.Dorfbewohner3000.Main;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

public class cmd_setteleporter implements CommandExecutor{
    File file = new File("plugins/ALSystem/", "config.yml");
    FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if(sender instanceof Player){
            Player p = (Player) sender;
            if(p.hasPermission("Lobby.Teleporter.set")){
                if(!(args.length >= 4)){
                    p.sendMessage("§cUse: /setteleporter (slot) (name) (item-id) (Command)");
                }else{
                    String slot = args[0];
                    String name = args[1];
                    String id = args[2];
                    String Command = "";
                    for(int i = 3; i < args.length; i++){
                        Command = Command+args[i]+" ";
                    }
                    cfg.set("Teleporter.Item.TSS-"+slot+".id", "ID"+id);
                    cfg.set("Teleporter.Item.TSS-"+slot+".cmd", Command);
                    cfg.set("Teleporter.Item.TSS-"+slot+".name", name);
                    try {
                        cfg.save(file);
                    }catch(IOException e){
                        e.printStackTrace();
                    }
                    p.sendMessage("§c§lSuccess");
                }
            }
        }
        return true;
    }
}
