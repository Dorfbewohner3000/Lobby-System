package de.Dorfbewohner3000.Main;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import de.Dorfbewohner3000.Events.damageevent;

public class cmd_build implements CommandExecutor{
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] strings) {
        if(cmd.getName().equalsIgnoreCase("build")){
            if(sender.hasPermission("lobby.build")){
            Player p = (Player) sender;
            if(damageevent.build.contains(p)){
                damageevent.build.remove(p);
                p.setGameMode(GameMode.ADVENTURE);
                p.sendMessage(Main.buildmodedisabled);
            }else {
                p.setGameMode(GameMode.CREATIVE);
                damageevent.build.add(p);
                p.sendMessage(Main.buildmodeenabled);
            }
            }
        }
        return true;
    }
}
