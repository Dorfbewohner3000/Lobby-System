package de.Dorfbewohner3000.Main;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

public class cmd_setwarp implements CommandExecutor{
    File file = new File("plugins/ALSystem/", "warps.yml");
    FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if(sender instanceof Player){
            Player p = (Player) sender;
            Location loc = p.getLocation();
            if(p.hasPermission("Lobby.Setwarp") & args.length == 1)
            {
                if(!file.exists()){
                    try {
                        file.createNewFile();
                    }catch(IOException e){
                        e.printStackTrace();
                    }
                }
                double x = loc.getX();
                double z = loc.getZ();
                double y = loc.getY();
                double yaw = loc.getYaw();
                double pitch = loc.getPitch();
                cfg.set("Warps."+args[0]+".X", x);
                cfg.set("Warps."+args[0]+".Y", y);
                cfg.set("Warps."+args[0]+".Z", z);
                cfg.set("Warps."+args[0]+".YAW", yaw);
                cfg.set("Warps."+args[0]+".PITCH", pitch);
                try {
                    cfg.save(file);
                }catch(IOException e){
                    e.printStackTrace();
                }
                p.sendMessage("§c§lSuccess");
            }

        }

        return true;
    }
}
